import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validator, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../../core/service/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  title = ''
  form: FormGroup;
  messageCrearUsuario: string;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private toastrService:ToastrService
    ) {}

  ngOnInit() {
    this.builbForm();
  }

  register(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      const value = this.form.value;
      console.log(this.form.value);
      this.authService.createUser(value.email, value.password)
      .then((creado) => {
           console.log('creado firebase');
           this.authService.crearUsuarioCliente(this.form.value).subscribe((user) => {
                  console.log('creado en posgrest');
                  this.router.navigate(['/auth/login']);
          });
       })
      .catch((error) => {
        this.messageCrearUsuario=""
        if(error.message == "The email address is badly formatted." )
          this.messageCrearUsuario ="Favor de ingresar un correo valido"
        if(error.message == 'Password should be at least 6 characters')
          this.messageCrearUsuario =  'La contraseña debe tener al menos 6 caracteres'
        if(error.message == 'The email address is already in use by another account.')
          this.messageCrearUsuario = 'La dirección de correo electrónico ya está siendo utilizada por otra cuenta.'
        this.toastrService.warning(error.message);
        console.log(error);
      });
    }
  }
  private builbForm() {
    this.form = this.formBuilder.group({
     username: ['', Validators.required],
     email : ['', Validators.required],
     password: ['', Validators.required],
     enabled: [true],
     tipousuario: ['EMPLEADO'],
     sucursalid : [0, Validators.required]
    });
  }
}
