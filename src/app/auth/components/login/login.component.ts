import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../core/service/auth.service';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formlogin: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private toastrService:ToastrService
  ) { }

  ngOnInit() {
    this.builbForm();
  }

  login(event: Event) {
   event.preventDefault();
   if (this.formlogin.valid) {
        const value = this.formlogin.value;
        this.authService.login(value.email, value.password)
        .then(() => {
          console.log('Autentifico firebase');
          this.authService.getUserByEmail(value.email)
          .subscribe((usuario) => {
            console.log('Autentifico api curiel');
            console.log(usuario);
            window.localStorage.setItem('sucursalId', usuario.sucursalid.toPrecision());
            window.localStorage.setItem('usuarioId', usuario.id.toPrecision());
            window.localStorage.setItem('usuario', usuario.username);
            this.router.navigate(['/admin']);
          });
        })
        .catch((error) => {
          let mesage ='';
          if ('The password is invalid or the user does not have a password.'== error.message) {
            mesage = 'La contraseña no es válida o el usuario no tiene contraseña.'
          }
          if ('There is no user record corresponding to this identifier. The user may have been deleted.' ==error.message) {
            mesage = 'No hay ningún registro de usuario que corresponda a este identificador. Es posible que se haya eliminado al usuario'
          }
           this.toastrService.warning(mesage);
           console.log('error.message', error.message)
         });
      }
  }
  private builbForm() {
    this.formlogin = this.formBuilder.group({
     email : ['', [Validators.required, Validators.email]],
     password: ['', Validators.required]
    });
  }

}
