import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ProductosComponent } from './components/producto/productos/productos.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ProductoDetalleComponent } from './components/producto/producto-detalle/producto-detalle.component';
import { LayoutComponent } from './components/layout/layout.component';
import { LayoutAuthComponent } from './auth/components/layout-auth/layout-auth.component';
import { AdminGuard } from './guard/admin.guard';
import { SucursalesComponent } from './components/sucursal/sucursales/sucursales.component';
import { UsuarioComponent } from './components/usuario/usuario/usuario.component';
import { PerfilComponent } from './components/usuario/perfil/perfil.component';
 
const routes: Routes = [
  {
   path: '',
   component : LayoutComponent,
   children: [
     {
      path: '',
      redirectTo: '/productos',
      pathMatch: 'full',
     },
    {
      path: 'home',
      component: HomeComponent
     },
     {
       path: 'productos',
       component: ProductosComponent
     },
     {
      path: 'productos/page/:page',
      component: ProductosComponent
     },
     {
      path: 'productosbysurcursal/:id',
      component: ProductosComponent
     },
     {
      path: 'productos/:id',
      component: ProductoDetalleComponent
     },
     {
      path: 'contacto',
      component: ContactoComponent
     },
     {
      path: 'usuario',
      component: UsuarioComponent
     }
     ,
     {
      path: 'perfil',
      component: PerfilComponent
     },
     {
      path: 'order',
      loadChildren : () => import ('./orden/orden.module').then( m => m.OrdenModule)
     },

     {
       path: 'sucursal',
       component: SucursalesComponent
     }
   ]
  },
  {
    path: 'admin',
    canActivate: [AdminGuard],
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)
  },
  {
    path: 'auth',
    component : LayoutAuthComponent,
   loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
  ,
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    scrollPositionRestoration: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
