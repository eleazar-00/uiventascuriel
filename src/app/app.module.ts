import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HomeComponent } from "./components/home/home.component";
import { ProductosComponent } from "./components/producto/productos/productos.component";
import { ContactoComponent } from "./components/contacto/contacto.component";
import { PageNotFoundComponent } from "./components/page-not-found/page-not-found.component";
import { ProductoDetalleComponent } from "./components/producto/producto-detalle/producto-detalle.component";
import { ProductoComponent } from "./components/producto/producto/producto.component";
import { LayoutComponent } from "./components/layout/layout.component";

import { SharedModule } from "@shared/shared.module";
import { HeaderComponent } from "@shared/components/header/header.component";
import { BannerComponent } from "@shared/components/banner/banner.component";
import { FooterComponent } from "@shared/components/footer/footer.component";
import { ExponentialPipe } from "@shared/pipes/exponential/exponential.pipe";
import { HighlightDirective } from "@shared/directives/highlight/highlight.directive";

import { MaterialModule } from "@material/material.module";
import { SucursalesComponent } from "./components/sucursal/sucursales/sucursales.component";
import { SucursalComponent } from "./components/sucursal/sucursal/sucursal.component";
import { environment } from "../environments/environment";
import { UsuarioComponent } from "./components/usuario/usuario/usuario.component";
import { PerfilComponent } from "./components/usuario/perfil/perfil.component";
import { DialogModalReferencaDePagoComponent } from './components/usuario/perfil/dialogModal/dialog-modal-referenca-de-pago/dialog-modal-referenca-de-pago.component';
import { AlertComponent } from './util/components/alert/alert.component';
import { ToastrModule } from 'ngx-toastr';
import { LoginComponent } from './components/login/login.component';
import { LayoutAuthComponent } from './auth/components/layout-auth/layout-auth.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProductosComponent,
    ContactoComponent,
    PageNotFoundComponent,
    ProductoDetalleComponent,
    ProductoComponent,
    LayoutComponent,
    HeaderComponent,
    FooterComponent,
    BannerComponent,
    ExponentialPipe,
    HighlightDirective,
    SucursalesComponent,
    SucursalComponent,
    UsuarioComponent,
    PerfilComponent,
    DialogModalReferencaDePagoComponent,
    AlertComponent,
    LoginComponent,
    LayoutAuthComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    SharedModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireStorageModule,
    ToastrModule.forRoot()
  ],
  entryComponents: [DialogModalReferencaDePagoComponent,LoginComponent,AlertComponent],
  providers: [ ],
  bootstrap: [AppComponent]
})
export class AppModule {}
