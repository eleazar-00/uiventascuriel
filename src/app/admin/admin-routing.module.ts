import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductoFormComponent } from './components/producto/producto-form/producto-form.component';
import { NavComponent } from './components/nav/nav.component';
import { ProductoListComponent } from './components/producto/producto-list/producto-list.component';
import { UnidadTablaComponent } from './components/unidad/unidad-tabla/unidad-tabla.component';
import { LineaComponent } from './components/linea/linea.component';
import { MovimientosComponent } from './components/movimiento/movimientos/movimientos.component';
import { VentaComponent } from './components/operacion/venta/venta.component';


const routes: Routes = [
  {
    path: '',
    component: NavComponent,
    children: [
      {
        path: 'productos',
        component: ProductoListComponent
      },
      {
        path: 'productos/page/:page/activos/:activo/online/:online',
        component: ProductoListComponent
      }, 
      {
        path: 'productoslist/create',
        component: ProductoFormComponent
      },
      {
        path: 'productoslist/create/:id',
        component: ProductoFormComponent
      },
      {
        path: 'unidadlist',
        component: UnidadTablaComponent
      },
      {
        path: 'movimientos',
        component: MovimientosComponent
      },
      {
        path: 'movimientos/page/:page',
        component: MovimientosComponent
      },
      {
        path: 'linea',
        component: LineaComponent
      },
      {
        path: 'venta',
        component: VentaComponent
      } 
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
