import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { ProductoFormComponent } from './components/producto/producto-form/producto-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { NavComponent } from './components/nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { ProductoListComponent } from './components/producto/producto-list/producto-list.component';
import { UnidadTablaComponent } from './components/unidad/unidad-tabla/unidad-tabla.component';
import { LineaComponent } from './components/linea/linea.component';
import { CategoriaComponent } from './components/linea/categoria/categoria.component';
import { MatTreeModule } from '@angular/material/tree';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
  import { MovimientosComponent } from './components/movimiento/movimientos/movimientos.component';
import { DetalleMovimientoComponent } from './components/movimiento/detalle-movimiento/detalle-movimiento.component';
import { VentaComponent } from './components/operacion/venta/venta.component';
 


@NgModule({
  declarations: [
     ProductoFormComponent, 
     NavComponent,  
     ProductoListComponent, UnidadTablaComponent, LineaComponent, CategoriaComponent, MovimientosComponent, DetalleMovimientoComponent, VentaComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    LayoutModule,
    MatTreeModule,
    MatIconModule,
    MatButtonModule 
    ],
  exports: [
     
  ],
  entryComponents: [DetalleMovimientoComponent]
})
export class AdminModule { }
