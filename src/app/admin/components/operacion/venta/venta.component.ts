import { Component, OnInit } from '@angular/core';
import { ProductoService } from "@core/service/producto.service";
import { Movimiento } from "@core/model/movimiento";
import { DetalleMov } from '@core/model/detalleMov';
import { ToastrService } from 'ngx-toastr';
import { FormaPagoService } from '@core/service/forma-pago.service'
import { MovimientoService } from "@core/service/movimiento.service";
import { PdfMakeWrapper, Table } from 'pdfmake-wrapper';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
//import pdfFonts from "pdfmake/build/vfs_fonts";

export class ProductoVenta {
  id: number;
  descripcion: string;
  precio: number;
  importe: number;
  cantidad: number;
 }
 
 type tableRow = [number, string, number, number];

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.scss']
})

export class VentaComponent implements OnInit {
   
  productos: ProductoVenta [] = [];
  totalImporteProductos:number = 0;
  formasDePagos:any;
  banderaAplicandoVenta =  false;
  banderaBuscandoProducto = false;
  constructor(private productoService:ProductoService,
              private toastrService:ToastrService,
              private formaPagoService: FormaPagoService,
              private movimientoService:MovimientoService) { 
               if(localStorage.getItem('ventaPos')) {
                  this.productos = JSON.parse(localStorage.getItem('ventaPos'));
                  for(let prod of this.productos) {
                    this.totalImporteProductos += prod.importe;
                  }
               }
              }

  ngOnInit() {
    this.getFormasDePago();
  }
  values = '';
  buscarProducto =(event) => {
    this.values = event.target.value;
    this.banderaBuscandoProducto = true;
    this.productoService.getByCodigodbarras(this.values)
                        .subscribe(
                          response => {
                             if(response.id)  {
                                var nuevoProducto:ProductoVenta = new ProductoVenta();
                                let idExiteListaProductos = this.productos.find(pro=>pro.id==response.id);
                                if(idExiteListaProductos){
                                  let indexProductoYaExite= this.productos.indexOf(idExiteListaProductos);
                                  this.totalImporteProductos-=idExiteListaProductos.importe;
                                  this.productos[indexProductoYaExite].cantidad+=1;
                                  this.productos[indexProductoYaExite].importe = this.productos[indexProductoYaExite].cantidad*response.precio 
                                  this.totalImporteProductos+=this.productos[indexProductoYaExite].importe;
                                } else {
                                  nuevoProducto.id = response.id;
                                  nuevoProducto.descripcion = response.descripcion;
                                  nuevoProducto.precio = response.precio;
                                  nuevoProducto.cantidad = 1;
                                  nuevoProducto.importe = response.precio * 1;
                                  this.productos.push(nuevoProducto);  
                                  this.totalImporteProductos+= nuevoProducto.importe
                                }
                             }
                             localStorage.setItem('ventaPos', JSON.stringify(this.productos));
                             this.banderaBuscandoProducto= false;
                          },
                          error =>{
                            this.banderaBuscandoProducto= false;
                            alert('Ocurrio un error intentalo nuevamete');
                          } )
   }
   removeProducto(productoRemover:ProductoVenta) {
     let indexProductoRemover = this.productos.indexOf(productoRemover);
     this.productos.splice(indexProductoRemover,1);
     this.totalImporteProductos-=productoRemover.importe;
     localStorage.setItem('ventaPos', JSON.stringify(this.productos));
   }
   
   aplicarVenta(){
     if(this.productos.length>0) {
      this.guardarMovimiento();
     } else {
      this.toastrService.warning('No a agregado ningun producto en la bandeja de venta')
     }
   }

  async guardarMovimiento() {
    this.banderaAplicandoVenta = true;
     let movimiento =  new Movimiento();
     movimiento.system = 'POS'
     movimiento.status = 'ENTREGADO'
     movimiento.clienteProveedor = 'Cliente de prueba'
     movimiento.observacion = ''
     movimiento.descripcion = 'Puntu de venta'
     movimiento.total = this.totalImporteProductos;
     let detallesMovimiento:DetalleMov[] = [];
      this.productos.forEach(productoVenta=> {
      let detalleMovimiento = new DetalleMov(); 
      detalleMovimiento.importe = productoVenta.precio;
      detalleMovimiento.cantidad = productoVenta.cantidad;
      detalleMovimiento.producto = productoVenta;
      detallesMovimiento.push(detalleMovimiento)
     })

     const movimientoFormaPago=[{
      "comprobantePago":``,
      "descripcionPago":`${this.formasDePagos[0].nombre}`,
      "formaDePago":this.formasDePagos[0],
      "pago":this.totalImporteProductos
     }];
     movimiento.detallesMov = detallesMovimiento;
     movimiento.movimientoFormaPago = movimientoFormaPago;
     console.log('Movimiento, ', movimiento);
     try {
          const reponseMovimiento = this.movimientoService.create(movimiento).toPromise();
          console.log('RespoinseMovimiento', reponseMovimiento);
          this.totalImporteProductos = 0
          this.productos = [];
          this.toastrService.success('Venta aplicada Correctamente')

     } catch (e) {
       this.toastrService.warning('Ocurrio un error al intentar aplicar la venta intenta de nuevo')
     } finally {
      localStorage.removeItem('ventaPos')
      this.banderaAplicandoVenta = false;
     }
   }

   async getFormasDePago() {
      try{
        const response= await this.formaPagoService.getFormasDePagoByEstatus().toPromise();
        this.formasDePagos=response;
        console.log("response formas de pago",response)
     }catch(e){
      console.log("Error getFormasPago",e)
     } 
   }


   async generarNota() {
    PdfMakeWrapper.setFonts(pdfFonts);
    const pdf = new PdfMakeWrapper();
    const data = this.productos;
    pdf.add('TIENDA CURIEL');
    pdf.add('Productos');
    pdf.add(this.createTable());
    pdf.add('**Agradecemos tu preferencia**');
    pdf.create().download();
   } 

   createTable() {
     return new Table([['Cantidad','Producto', 'Precio/Unitario', 'Importe'],...this.extractData(),['','','Total $',this.totalImporteProductos]]).end;
   }

   extractData():tableRow[] {
     return this.productos.map(row => [row.cantidad, row.descripcion, row.precio, row.importe])
   }

}
