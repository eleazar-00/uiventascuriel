import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AuthService } from '../../../core/service/auth.service';
import { Router } from '@angular/router';
import { SucursalService } from '../../../core/service/sucursal.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {
  empleado: string;
  sucursal: string;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private authService: AuthService,
    private router: Router,
    private sucursalService: SucursalService
    ) {
      this.empleado = localStorage.getItem('usuario');
      const sucursalid = localStorage.getItem('sucursalId');
      this.fecthSucursal(parseInt(sucursalid) );
    }
  logout() {
    this.authService.logout()
    .then(() => {
      localStorage.removeItem('sucursalId');
      localStorage.removeItem('usuarioId');
      localStorage.removeItem('sucursal');
      this.router.navigate(['/productos']);
    });
  }
  fecthSucursal(id: number) {
   this.sucursalService.getSucursalById(id)
   .subscribe((sucursal) => {
      this.sucursal = sucursal.nombre;
   });
  }
}
