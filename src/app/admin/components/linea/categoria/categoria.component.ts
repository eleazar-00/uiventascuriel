import { Component,Input, Output, EventEmitter } from '@angular/core';
import {LineaService} from '@core/service/linea.service'

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.scss']
})
export class CategoriaComponent {
  @Input() categoria: any;
  @Output() eliminadoDepartamento = new EventEmitter<string>();
  @Output() addCategoria = new EventEmitter<string>();


  constructor(private lineaService:LineaService ) { 
    console.log('linea', this.categoria);
  } 


  deleteDepartamento = (id) => {
    this.lineaService.delete(id).subscribe(
                       response=>{
                         console.log(response);
                         alert('Eliminado Correctamente');
                         this.eliminadoDepartamento.emit('eliminadoDepartamento')
                       }
                       ,error=> {
                         console.log('error', error);
                         alert('Ocurrio error al intentar eliminar un departamento')
                       }
                       )
  }
  addCategorias = (datos) => {
    this.addCategoria.emit(datos);
  }

}