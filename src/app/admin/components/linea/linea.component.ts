import { Component, OnInit } from '@angular/core';
import {LineaService} from '@core/service/linea.service'
import { FormBuilder ,FormGroup,Validators} from '@angular/forms';
import { Categoria } from '@core/model/categoria'
  
@Component({
  selector: 'app-linea',
  templateUrl: './linea.component.html',
  styleUrls: ['./linea.component.scss']
})
export class LineaComponent implements OnInit {
  formLinea:FormGroup;
  crearNuevo:boolean = false;
  addCat:boolean = false;
  departamentoEditar:Categoria;

  selecionoPadre:boolean=false;
  categoriaSeleccionado:Categoria;
  categorias:Categoria[] = [];

  constructor(private lineaService:LineaService,private formBuilder:FormBuilder) { }

  ngOnInit() {
    this.getAll()
    this.buildForm();
  }

  private buildForm() {
    this.formLinea = this.formBuilder.group({
    id: [0 ] , 
    descripcion: ['' , [Validators.required]],
    status: [1],
    categorias: []
  });
} 
 
getAll=()=>{
    this.lineaService.getAll().subscribe(
      categorias=>{
        this.categorias = categorias.filter(cat => cat.categorias.length>0);
        let subCategoriaId=[];
        for (let i =0; i< this.categorias.length;i++) {
          for(let subCat of this.categorias[i].categorias) {
            subCategoriaId.push(subCat.id)
          }
         }
        let departamentoSinCat:Categoria[] = categorias.filter(cat => cat.categorias.length==0 && !subCategoriaId.includes(cat.id)); 
        this.categorias = [...this.categorias, ...departamentoSinCat];
       },
      error=>{
        console.log('error',error)
      }

    )
}
save(event: Event) {
    event.preventDefault();
      if (this.formLinea.valid) {
        const categoria = this.formLinea.value 
        this.lineaService.saves(categoria)
         .subscribe((newproducto) => {
                   this.crearNuevo =false;
                   this.getAll();
                   alert('Creado correctamente')
              });
      }
     
  } 
  
 nuevo() {
   this.crearNuevo = true;
 }
 cancelar(event) {
  event.preventDefault();
  this.crearNuevo = false;
  this.addCat = false;
  }

 getDepartamentos(mensaje) {
    this.getAll();
 }

 addCategoria(datosDepCat){
  this.addCat =true;
  this.departamentoEditar = datosDepCat;
  } 

  async addSubCategoria(event) {
    event.preventDefault();
    const responseAddCategoria= await this.lineaService.saves(this.formLinea.value).toPromise();
    this.departamentoEditar.categorias.push(responseAddCategoria);
    const responseAddUpdate = await this.lineaService.update(this.departamentoEditar).toPromise();
  }
  removeSubcategoria(index, id) {
    if(id) {
      this.departamentoEditar.categorias.splice(index, 1);
      this.lineaService.update(this.departamentoEditar)
                       .subscribe((newproducto) => {
                           this.lineaService.delete(id)
                                          .subscribe(
                                            response => {
                                             console.log(response);
                                             this.getAll(); 
                                             alert('Eliminado Correctamente');
                                            }
                                          ,error => {
                                             console.log('error', error);
                                           alert('Ocurrio error al intentar eliminar un departamento')
                                          }
                         )
                      });
    }
  }
}
