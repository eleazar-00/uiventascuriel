import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ProductoService} from '../../../../core/service/producto.service';
import { CategoriaService } from '../../../../core/service/categoria.service';
import { UnidadService } from '../../../../core/service/unidad.service';
import { Categoria } from 'src/app/core/model/categoria';
import { Unidad } from '../../../../core/model/unidad';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { finalize } from 'rxjs/operators'
import { MyValidators} from '../../../../util/validators';
import { AngularFireStorage } from '@angular/fire/storage';
import { pipe, Observable, from } from 'rxjs';
import { IfStmt } from '@angular/compiler';

@Component({
  selector: 'app-producto-form',
  templateUrl: './producto-form.component.html',
  styleUrls: ['./producto-form.component.scss']
})
export class ProductoFormComponent implements OnInit {
  form: FormGroup;
  categorias: Categoria[] = [];
  unidades: Unidad[] = [];
  selectedUnidad:Unidad;
  selectedCategoria: Categoria;
  imagen$: Observable<any>;
  imagenEditar: string;
  buttonSave = false;
  buttonUpdate = false;
  constructor(
    private formBuilder: FormBuilder,
    private productoService: ProductoService,
    private categoriaService: CategoriaService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private angularFireStorage: AngularFireStorage,
    private unidadService: UnidadService
  ) {
      this.buildForm();
      const sucursal = localStorage.getItem('sucursalId');
      const usuario = localStorage.getItem('usuarioId');
      this.form.get('usuario').setValue({id: usuario});
      this.form.get('sucursal').setValue({id: sucursal });
      }

 ngOnInit() {
     this.activatedRoute.params.subscribe((params: Params) => {
       const id = params.id;
       if (id) {
        this.buttonUpdate = true;
        this.fecthProductoById(id);
       } else {

         this.fetchCategoria();
         this.fetchUnidades();
         this.buttonSave = true;
       }
     });
  }
 async fecthProductoById(id){
  await this.fetchCategoria();
  await this.fetchUnidades();
  let producto = await this.productoService.getById(id).toPromise();
  this.form.patchValue(producto);
  let idCategoria = 0;
  let idSubcategoria = 0;
  for(let i = 0;  i < this.categorias.length && idSubcategoria<=0;i++){
    idCategoria = this.categorias[i].id
    for(let j=0;j<this.categorias[i].categorias.length && idSubcategoria<=0 ;j++) {
          if (producto.categoria.id == this.categorias[i].categorias[j].id) {
               idSubcategoria = this.categorias[i].categorias[j].id;
               this.selectedCategoria = this.categorias[i].categorias[j]
          }
    }
  }  
  this.selectedUnidad = this.unidades.find(unidad => unidad.id==producto.unidadMedida.id);
  this.imagenEditar = producto.imagen;
 } 
   
  saveProducto(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
        const producto = this.form.value 
        this.productoService.create(producto)
         .subscribe((newproducto) => {
                 console.log(newproducto);
                 this.router.navigate(['./admin/productos']);
             });
      }
  }

  updateProducto(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
        const producto = this.form.value 
        this.productoService.create(producto)
         .subscribe((newproducto) => {
                 console.log(newproducto);
                 this.router.navigate(['./admin/productos']);
             });
      }
  }

  updateFaile(event) {
       const file = event.target.files[0];
       const dir = file.name;
       const fileRef = this.angularFireStorage.ref(dir);
       const taskSubirImagen = this.angularFireStorage.upload(dir, file);
       taskSubirImagen.snapshotChanges()
       .pipe(
            finalize(() => {
             this.imagen$ = fileRef.getDownloadURL();
             this.imagen$.subscribe(url => {
                this.form.get('imagen').setValue(url);
             });
            })
       )  
       .subscribe();
       console.log(file);
  }

  private buildForm() {
      this.form = this.formBuilder.group({
      id: [0 ] ,
      nombre: ['' , [Validators.required]],
      descripcion: ['' , [Validators.required]],
      codigoBarras:[],
      status: [1],
      precio: [0 , [Validators.required, MyValidators.isPrecioValid]],
      costo: [0 , [Validators.required, MyValidators.isPrecioValid]],
      imagen: [''],
      visiblePage: [ false],
      fechaCreacion: [''],
      fechaModificacion: [''],
      sucursal: [],
      usuario: [] ,
      categoria: [],
      unidadMedida: [null],
      existencias: [0],
      apartados : [0],
      palabrasClave: [],
      observaciones:[]
    });
  } 

  get priceField() {
    return this.form.get('precio');
  }

 async fetchCategoria() {
   this.categorias = await this.categoriaService.getCategorias(1).toPromise();
  }

  async fetchUnidades() {
   this.unidades = await this.unidadService.getAll().toPromise();
  }
}