import { Component, OnInit } from "@angular/core";
import { ProductoService } from "@core/service/producto.service";
import { Router ,ActivatedRoute} from "@angular/router"; 
import { ToastrService } from 'ngx-toastr';
import { CategoriaService } from '@core/service/categoria.service';

@Component({
  selector: "app-producto-list",
  templateUrl: "./producto-list.component.html",
  styleUrls: ["./producto-list.component.scss"]
})
export class ProductoListComponent implements OnInit {
  //Filtro
  checkedProductosActivos = true;
  checkedVisibleProductoEnTiendaOnline = false;
  selectTipoBusqueda;
  selectCategoria;
  nombreProductoBuscar:string;

  productos = [];
  categorias = [];
  banderaBuscandoProducto: boolean = false;
  displayedColumns: string[] = ["id","codigoBarras","nombre", "precio", "visiblePage" ,"existencias","apartado",  "acciones"];
  
  // MatPaginator Inputs
  page = 0;
  length = 0;
  pageSize = 50;
  pageSizeOptions: number[] = [10];
 

  constructor(
    private productoService: ProductoService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private categoriaService:CategoriaService
  ) { 

  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params=>{
      this.page = (params.get("page")) ?  Number(params.get("page")): this.page;
      this.checkedProductosActivos =  (params.get("activo")=='true' || !params.get("activo")) ?  true: false;
      this.checkedVisibleProductoEnTiendaOnline =  (params.get("online")=='true') ? true:false;
      this.fetchProducto(); 
    });
    this.fetchCategoria();
  }

  fetchProducto() {
    this.banderaBuscandoProducto = true;
    if (!this.selectTipoBusqueda || this.selectTipoBusqueda=='ninguno') {
      this.productoService.getAll(this.page, this.pageSize, Number(this.checkedProductosActivos), this.checkedVisibleProductoEnTiendaOnline).subscribe(producto => {
        this.productos = producto.content;
        this.length = producto.totalElements;
        this.banderaBuscandoProducto = false;
      });
    }
    if(this.selectTipoBusqueda == 'nombre') {
      if(this.nombreProductoBuscar){
       this.productoService.getByNombreAndStatus(this.nombreProductoBuscar.toUpperCase(), this.page, this.pageSize, Number(this.checkedProductosActivos), this.checkedVisibleProductoEnTiendaOnline)
                                .subscribe(producto=>{
                                  this.productos = producto.content;
                                  this.length = producto.totalElements;
                                  this.banderaBuscandoProducto = false; 
                                })
      } else {
         this.banderaBuscandoProducto = false; 
         this.toastrService.warning('Ingrese alguna descripción para buscar el producto');
      }
     }

    if(this.selectTipoBusqueda == 'departamento') {
      if (this.selectCategoria) {
        this.productoService.getByCategoriaIdAndStatus(this.selectCategoria.id, this.page, this.pageSize, Number(this.checkedProductosActivos), this.checkedVisibleProductoEnTiendaOnline)
        .subscribe(producto=>{
          this.productos = producto.content;
          this.length = producto.totalElements;
          this.banderaBuscandoProducto = false; 
        })
      } else {
        this.banderaBuscandoProducto = false; 
        this.toastrService.warning('Selecciona un departamento')
      }
    }
  }

  onPaginateChange(event) {
    this.router.navigate([`admin/productos/page/${event.pageIndex}/activos/${this.checkedProductosActivos}/online/${this.checkedVisibleProductoEnTiendaOnline}`]);
  }

  deleteProducto(id) {
    if (confirm("Confirmar para eliminar el producto!")) {
      this.productoService.delete(id).subscribe(
        rpt => {
          this.toastrService.success('Producto Eliminado Correctamente');
          this.fetchProducto();
        },
        error => {
          this.toastrService.warning("Ocurrio un error al intentar elminiar un producto, contacta al administrador del sistema="+error.message);
        }
      );
    }
  } 


  async fetchCategoria() {
    this.categorias = await this.categoriaService.getCategorias(1).toPromise();
   }
}
