import { Component, OnInit } from '@angular/core';
import { MovimientoService} from '@core/service/movimiento.service';
import { Movimiento } from '@core/model/movimiento';
import { MatDialog } from '@angular/material/dialog';
import { DetalleMovimientoComponent } from '../detalle-movimiento/detalle-movimiento.component';
import { Router ,ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-movimientos',
  templateUrl: './movimientos.component.html',
  styleUrls: ['./movimientos.component.scss']
})
export class MovimientosComponent implements OnInit {
  displayedColumns: string[] = ["id","dateCreated","system","clienteProveedor","descripcion", "status", "acciones"];

  movimientos:Movimiento [] =[];

  // MatPaginator Inputs
  page = 0;
  length = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 20, 30];
   
  constructor(private movimientoSerive:MovimientoService,
              public dialog: MatDialog,
              private router:Router,
              private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
     this.activatedRoute.paramMap.subscribe(params =>{
        let pageParams = params.get('page') 
        this.page = (pageParams) ? Number(pageParams) : this.page;
        this.getAllMovimientos();
     })
  }

  getAllMovimientos(){
    this.movimientoSerive.getAll(this.pageSize, this.page).subscribe(
          movimiento=>{
            this.movimientos=movimiento['content']
            this.length = movimiento['totalElements']
          },
          error=>{
            console.log('Error getAllMovimientos',error)
          }
    )
  }
  onPaginateChange(event) {
     this.router.navigate(["admin/movimientos/page/" + event.pageIndex]);
  }
  openDialog(movimiento) {
    const dialogRef = this.dialog.open(DetalleMovimientoComponent,{
      data: {
        movimiento: movimiento
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getAllMovimientos();
      console.log(`Dialog result: ${result}`);
    });
  }

}
