import { Component, OnInit,Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MovimientoService } from '@core/service/movimiento.service';
import { ToastrService } from 'ngx-toastr';

export interface DialogData {
  movimiento: any;
}

@Component({
  selector: 'app-detalle-movimiento',
  templateUrl: './detalle-movimiento.component.html',
  styleUrls: ['./detalle-movimiento.component.scss']
})
export class DetalleMovimientoComponent implements OnInit {
  estatus:string;
  referenciaPago:any;
  comprobantePago:string;
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData,
                                       private movimientoService:MovimientoService,
                                       private toastrService:ToastrService) {

                                        }

  ngOnInit() {
    this.comprobantePago='';
    this.movimientoService.getFormaDePagoByMovimiento(this.data.movimiento.id)
                           .subscribe(response =>{
                             this.referenciaPago = response[0];
                             this.comprobantePago = this.referenciaPago.comprobantePago;
                             console.log('Referencia de pago', response);
                           })
    this.estatus = this.data.movimiento.status;
    console.log(this.data.movimiento, 'movimiento modal');
  }

  updateStatus(status){
    const STATUS={"status":status}
    const movimientoId = this.data.movimiento.id as number;
    this.movimientoService.updateStatusMovimiento(STATUS,movimientoId)
                          .subscribe(
                            response=>{
                            this.estatus = status;
                            alert(`Actualizado correctamente`)
                            console.log(response,"Status actualizado")
                          }
                          ,error => {
                            if(error.status == 200 ) {
                              this.estatus = status;
                              this.toastrService.success(`Movimiento actualizado a ${status}`);
                            } else {
                              this.toastrService.error(`Ocurrio un erro al intentar ${status}`);
                             }
                           }
                          )
                          

  }
}
