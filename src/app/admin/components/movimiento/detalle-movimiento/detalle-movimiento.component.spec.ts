import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleMovimientoComponent } from './detalle-movimiento.component';

describe('DetalleMovimientoComponent', () => {
  let component: DetalleMovimientoComponent;
  let fixture: ComponentFixture<DetalleMovimientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleMovimientoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleMovimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
