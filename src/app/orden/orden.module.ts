import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { OrdenRoutingModule } from "./orden-routing.module";
import { OrderComponent } from "./components/order/order.component";
import { MaterialModule } from "../material/material.module";
import { CartcountPipe } from "../components/shared/pipes/cartcount.pipe";
import { FormDatosComponent } from "./components/form-datos/form-datos.component";
import { ReactiveFormsModule } from "@angular/forms";
import { FormasDePagosComponent } from './components/formas-de-pagos/formas-de-pagos.component';
 import { ModalDialogFinalizarCompraComponent } from './components/dialog/modal-dialog-finalizar-compra/modal-dialog-finalizar-compra.component';
@NgModule({
  declarations: [OrderComponent, CartcountPipe, FormDatosComponent, FormasDePagosComponent, ModalDialogFinalizarCompraComponent],
  imports: [
    CommonModule,
    OrdenRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  entryComponents: [ModalDialogFinalizarCompraComponent,],
  exports: []
})
export class OrdenModule {}
