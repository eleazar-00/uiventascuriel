import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormaPago } from '@core/model/forma-pago'; 
import { FormaPagoService } from '@core/service/forma-pago.service'

@Component({
  selector: 'app-formas-de-pagos',
  templateUrl: './formas-de-pagos.component.html',
  styleUrls: ['./formas-de-pagos.component.scss']
})
export class FormasDePagosComponent implements OnInit {
  @Input() title: string;
  @Output() referenciaSeleccionadoEvent = new EventEmitter<boolean>();
  
  visibleFormaDePago:boolean=true;
  visibleReferenciaPago:boolean=false;
  otracantidad:number=0;

  formasDePagos:FormaPago[];
  formaDePagoSelecionado:FormaPago;
  
  referenciasDePago:any;
  referencaSeleccionado:any;

  constructor( private formaPagoService:FormaPagoService) { 
    this.getFormasPago();
  }
  ngOnInit() {
  }
  getFormasPago=async()=>{
    try{
       const response= await this.formaPagoService.getFormasDePagoByEstatus().toPromise();
       this.formasDePagos=response;
       console.log("response formas de pago",response)
    }catch(e){
      console.log("Error getFormasPago",e)
    }
  }

  getReferanciaDePago=async(formadePago)=>{
    this.formaDePagoSelecionado=formadePago
    console.log("Forma de pago Seleccionado",this.formaDePagoSelecionado)
    try {
        const response= await this.formaPagoService.getReferenciaFormaDePago(formadePago.id).toPromise();
        this.referenciasDePago=response;  
        if(this.referenciasDePago.length>0){
          this.visibleFormaDePago=false;
          this.visibleReferenciaPago=true
        }else{
          alert("Forma de Pago no activa, Favor de seleccionar algun otra Forma de Pago");
        }
    } catch (error) {
       console.log(error,"Error getReferanciaDePago ")  
    }
  }

  referenciaSeleccionado(referencia){
    console.log("Referencia seleccionado",referencia);
    this.referencaSeleccionado=referencia;
    this.referenciaSeleccionadoEvent.emit(true)
  }
  regresarFormaDePago(){
    this.otracantidad=0
    this.visibleFormaDePago=true;
    this.visibleReferenciaPago=false
    this.referenciaSeleccionadoEvent.emit(false)
  }
}
