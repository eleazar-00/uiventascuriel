import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormasDePagosComponent } from './formas-de-pagos.component';

describe('FormasDePagosComponent', () => {
  let component: FormasDePagosComponent;
  let fixture: ComponentFixture<FormasDePagosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormasDePagosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormasDePagosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
