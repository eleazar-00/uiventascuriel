import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
 
@Component({
  selector: 'app-form-datos',
  templateUrl: './form-datos.component.html',
  styleUrls: ['./form-datos.component.scss']
})
export class FormDatosComponent implements OnInit {
  formDatosEnvio: FormGroup;
  @Output()  datosenvios = new EventEmitter<any>();

  constructor(
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.buildFormDatos();
  }

  buildFormDatos() {
   this.formDatosEnvio = this.formBuilder.group({
       cliente : [, [Validators.required] ],
       telefono: [, [Validators.required]],
       direccion: [, [Validators.required]],
       referencia: [, [Validators.required]]
   });
  }
  asignarDatos(event: Event) {
    event.preventDefault();
    this.datosenvios.emit(this.formDatosEnvio.value);
   }
}
