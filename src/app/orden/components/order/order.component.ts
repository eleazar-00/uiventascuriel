import { Component, OnInit,ViewChild, AfterViewInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { STEPPER_GLOBAL_OPTIONS } from "@angular/cdk/stepper";
import { Producto } from "../../../core/model/producto.model";
import { Movimiento } from "@core/model/movimiento";
import { DetalleMov } from "@core/model/detalleMov";
import { CartService } from "../../../core/service/cart.service";
import { Observable, pipe, from } from "rxjs";
import { map } from "rxjs/operators";
import { Router } from "@angular/router";
import { MovimientoService } from "@core/service/movimiento.service";
import { FormasDePagosComponent } from '../formas-de-pagos/formas-de-pagos.component';
import {MatDialog} from '@angular/material/dialog';
import {ModalDialogFinalizarCompraComponent} from '../dialog/modal-dialog-finalizar-compra/modal-dialog-finalizar-compra.component';
import {LoginComponent} from '../../../components/login/login.component';
import { AuthService } from "@core/service/auth.service";

@Component({
  selector: "app-order",
  templateUrl: "./order.component.html",
  styleUrls: ["./order.component.scss"],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { showError: true }
    }
  ]
})
export class OrderComponent implements OnInit {

  @ViewChild(FormasDePagosComponent,{static:false}) formasDePagos;


  formDatoscliente: FormGroup;
  producto$: Observable<Producto[]>;
  total: number;
  movimiento: Movimiento;
  detalleMov: DetalleMov[] = [];
  detalle: DetalleMov;
  titleFormaDePago="Formas de Pago" 
  btnFinalizar=false
  logeado$: Observable<string>; 
  constructor(
    private formBuilder: FormBuilder,
    private cartService: CartService,
    private movimientoService: MovimientoService,
    public dialog: MatDialog,
    private router: Router,
    private authService: AuthService
  ) {
    window.scroll(0,0);
    this.logeado$ = this.authService.mensajeUsuarioLogeado$.pipe(
      map(mensaje => (localStorage.getItem("usuario") ? "mensaje" : mensaje))
    );
    if (localStorage.getItem("usuario")) {
       console.log('logeado');
    } else { this.openModaLogin();}
    this.producto$ = this.cartService.cart$.pipe(
      map((products: []) => {
        const distintos = [...new Set(products)];
        return distintos;
      })
    );
    this.sumaTotal();
  }
  ngOnInit() {    

    this.formDatoscliente = this.formBuilder.group({
      cliente: [, [Validators.required]],
      correo: [, [Validators.email]],
      celular: [, [Validators.required]],
      direccion: [, [Validators.required]],
      referencia: [, [Validators.required]]
    });
  } 
  

  sumaTotal() {
    this.total = 0;
    this.producto$.subscribe(produto => {
      produto.forEach(producto => {
        this.total += (producto.cantidad*producto.precio);
      });
    });
  }

  clickRellenarDatos() {
    if (this.total === 0) {
      alert("Elija un producto");
      this.router.navigate(["/productos"]);
    }
  }

  deleteProducto(producto) {
    this.cartService.deleteCart(producto);
    this.sumaTotal();
  }

  addProducto(producto){
     this.cartService.addCart(producto);
     this.sumaTotal();
  }

  clearProductos() {
    this.cartService.clearCart();
    this.sumaTotal();
  }

  aplicarCompra() {
    if(this.formasDePagos.formaDePagoSelecionado){
      if(this.formasDePagos.referencaSeleccionado){
        if(this.formasDePagos.referencaSeleccionado.numeroCuenta==='Digite la cantidad'){
            if(this.formasDePagos.otracantidad>=this.total){
              let descripcionPago=`${this.formasDePagos.referencaSeleccionado.decripcion} : $ ${this.formasDePagos.otracantidad}`
              this.construirDtoMovimiento(descripcionPago);
            }else{
              alert("La otra cantidad registrada debe de se mayor o igual al importe total de su compra")
            }
        }else {
           let descripcionPago=`${this.formasDePagos.referencaSeleccionado.decripcion} : ${this.formasDePagos.referencaSeleccionado.numeroCuenta}`
           this.construirDtoMovimiento(descripcionPago);
         }
      }else{
        alert("No as Seleccionado Ninguna referencia");
      }
    }else{
      alert("No as Seleccionado ninguna forma de pago")
     console.log("No as Seleccionado ninguna forma de pago")
    }
  }

  construirDtoMovimiento(descripcionPago){
     //CONSTRUIR DTO MOVIMIENTO
     console.log("cliente");
     console.log(this.formDatoscliente.value);
     console.log("Productos");
     this.movimiento = new Movimiento();
     let clienteProveedor = this.formDatoscliente.value.correo;
     if (localStorage.getItem("email"))
       clienteProveedor = localStorage.getItem("email");
     this.movimiento.clienteProveedor = clienteProveedor;
     this.movimiento.descripcion = "Compra online curiel";
     this.movimiento.system="WEB"
     this.movimiento.status="PROCESO"
     this.movimiento.observacion = `Cliente: ${this.formDatoscliente.value.cliente}, Correo: ${this.formDatoscliente.value.correo}, celular: ${this.formDatoscliente.value.celular}`;
     this.producto$.subscribe(productos => {
       this.detalleMov = [];
       for (const producto of productos) {
         this.detalle = new DetalleMov();
         this.detalle.cantidad = producto.cantidad;
         this.detalle.importe = producto.precio;
         this.detalle.producto = producto;
         this.detalleMov = [...this.detalleMov, this.detalle];
       }
     });
     this.movimiento.detallesMov = this.detalleMov;
     this.movimiento.total = this.total;
     const movimientoFormaPago=[{
       "comprobantePago":`${this.formasDePagos.otracantidad}`,
       "descripcionPago":`${descripcionPago}`,
       "formaDePago":this.formasDePagos.formaDePagoSelecionado,
       "movimiento":{},
       "pago":this.total
     }];
     this.movimiento.movimientoFormaPago=movimientoFormaPago
     console.log(this.movimiento);
     this.movimientoService.create(this.movimiento).subscribe(
       movimiento => {
         this.openDialogFinalizarCompra("Tu compra se registró Exitosamente",movimiento);
         this.clearProductos();
         this.router.navigate(["/productos"]);
       },
       error => {
         alert("Error");
         console.log(error);
       }
     );
   }


  openDialogFinalizarCompra(title,movimiento) {
    this.dialog.open(ModalDialogFinalizarCompraComponent,{
      height:'300px',
      data: {title:title, movimiento:movimiento}
    });
  }

  receiveMessage($event) {
    console.log('recibe eventos del hijo', $event)
    this.btnFinalizar = $event
  }

  openModaLogin=()=>{
    this.dialog.open(LoginComponent,{
      //height:'100%',
      disableClose: true,
      data: {dirigir:'/order'}
    });
  }
}
 