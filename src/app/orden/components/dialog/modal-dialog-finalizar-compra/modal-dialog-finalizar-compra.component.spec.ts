import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDialogFinalizarCompraComponent } from './modal-dialog-finalizar-compra.component';

describe('ModalDialogFinalizarCompraComponent', () => {
  let component: ModalDialogFinalizarCompraComponent;
  let fixture: ComponentFixture<ModalDialogFinalizarCompraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDialogFinalizarCompraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDialogFinalizarCompraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
