import { Component, OnInit,Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { Movimiento } from '@core/model/movimiento';

export interface DialogData {
  title: string;
  movimiento: Movimiento;
}

@Component({
  selector: 'app-modal-dialog-finalizar-compra',
  templateUrl: './modal-dialog-finalizar-compra.component.html',
  styleUrls: ['./modal-dialog-finalizar-compra.component.scss']
})
export class ModalDialogFinalizarCompraComponent implements OnInit {
   constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) { 

  }
  ngOnInit() {
    console.log("title", this.data.title)
  }

}
