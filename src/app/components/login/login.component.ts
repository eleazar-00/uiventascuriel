import { Component,OnInit,Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AuthService} from '@core/service/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

export interface DialogData {
  dirigir: string; 
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  login:boolean = false;
  constructor(public dialogRef:MatDialogRef<LoginComponent>,
                   @Inject(MAT_DIALOG_DATA) public data: DialogData, 
                                       private authService:AuthService,
                                       private router: Router,
                                       private toastrService: ToastrService) { }
  typeLogin = [//{id:1, name: "Faceboock" ,icono: "facebook", color: "primary"},
               {id:2, name: "Gmail" ,icono: "email", color: "warn"}];
  ngOnInit() {
  }

  autentificacion(type){
    if (type == 'Gmail') {
      this.authCuentaGoogle();
    }
    if (type == 'Faceboock') {
       this.authCuentafaceboock();
    }
  }
  authCuentafaceboock = () => {
    this.login = true;
    this.authService.loginFaceboock()
                  .then(response=>{
                   this.login = false;
                   this.authService.addUsuarioLogeado('Bienvanido');
                   this.dialogRef.close();
                    //window.localStorage.setItem('usuarioId', user.id.toPrecision());
                    window.localStorage.setItem('usuario', response.user.displayName);
                    window.localStorage.setItem('email', response.user.email);
                    window.localStorage.setItem('avatar', response.user.photoURL);
                    this.router.navigate([this.data.dirigir]);
                    this.toastrService.info(`Bienvenido ${response.user.displayName}`);
                   
                  }).catch( error => {
                    this.dialogRef.close();
                    this.login = false;
                    console.log("Error al autentificarse con faceboock", error);
                  })
  }
  email = () => {
    console.log('login email');
  }
  authCuentaGoogle = () => {
     this.login = true;
     this.authService.loginGoogle()
                   .then(response=>{
                    this.login = false;
                    this.authService.addUsuarioLogeado('Bienvanido');
                    this.dialogRef.close();
                    window.localStorage.setItem('usuario', response.user.displayName);
                    window.localStorage.setItem('email', response.user.email);
                    window.localStorage.setItem('avatar', response.user.photoURL);
                    this.router.navigate([this.data.dirigir]);
                    this.toastrService.info(`Bienvenido ${response.user.displayName}`);
                   }).catch( error => {
                    this.dialogRef.close();
                    this.login = false;
                    console.log("Error al autentificarse con Google", error);
                   })
  }


  onNoClick(): void {
    this.dialogRef.close();
    this.router.navigate(['/productos']);
  }
}
