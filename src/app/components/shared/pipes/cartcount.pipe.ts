import { Pipe, PipeTransform } from '@angular/core';
import { CartService } from '../../../core/service/cart.service';

@Pipe({
  name: 'cartcount'
})
export class CartcountPipe implements PipeTransform {
  constructor(private cartService: CartService) {
  }
  transform(product: any, args?: any): any {
    let total = 0;
    this.cartService.cart$.subscribe(products => {
      products.forEach((elemento) => {
        if (elemento.id === product.id) {
          total += 1;
        }
      });
    });
    return total;
  }

}
