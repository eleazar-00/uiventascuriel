import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-banner",
  templateUrl: "./banner.component.html",
  styleUrls: ["./banner.component.scss"]
})
export class BannerComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
  images: string[] = [
    "https://firebasestorage.googleapis.com/v0/b/mercadito-fff0d.appspot.com/o/curielbanner.png?alt=media&token=d2e2acc7-1695-4aed-93b4-8e6767d0110a",
    "https://firebasestorage.googleapis.com/v0/b/mercadito-fff0d.appspot.com/o/banner-mama.png?alt=media&token=b5f84c17-de11-4a53-a997-468fe3f4cba1"
  ];
}
