import { Component, OnInit } from "@angular/core";
import { CartService } from "../../../../core/service/cart.service";
import { map} from "rxjs/operators";
import { Observable} from "rxjs";
import { AuthService } from "../../../../core/service/auth.service";
import {MatDialog} from '@angular/material/dialog';
import {LoginComponent} from '../../../login/login.component';

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  total$: Observable<number>;
  logeado$: Observable<string>; 

  constructor(
    private cartService: CartService,
    private authService: AuthService,
     public dialog: MatDialog
  ) {
    this.total$ = this.cartService.cart$.pipe(
      map(productos => productos.length)
    );
    this.logeado$ = this.authService.mensajeUsuarioLogeado$.pipe(
      map(mensaje => (localStorage.getItem("usuario") ? "mensaje" : mensaje))
    );
  }
  ngOnInit() {}

  openModaLogin=()=>{
    this.dialog.open(LoginComponent,{
      //height:'100%',
      disableClose: true,
      data: {dirigir:'/perfil'}
    });
  }

}
