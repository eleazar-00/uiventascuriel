import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogModalReferencaDePagoComponent } from './dialog-modal-referenca-de-pago.component';

describe('DialogModalReferencaDePagoComponent', () => {
  let component: DialogModalReferencaDePagoComponent;
  let fixture: ComponentFixture<DialogModalReferencaDePagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogModalReferencaDePagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogModalReferencaDePagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
