import { Component, OnInit,Inject } from '@angular/core';
import { MAT_DIALOG_DATA, throwMatDialogContentAlreadyAttachedError} from '@angular/material/dialog';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators'
import { MovimientoService } from '@core/service/movimiento.service';

export interface DialogData {
  title: string;
  refenciaDePago: any;
  idMovimiento:number;
}
@Component({
  selector: 'app-dialog-modal-referenca-de-pago',
  templateUrl: './dialog-modal-referenca-de-pago.component.html',
  styleUrls: ['./dialog-modal-referenca-de-pago.component.scss']
})
export class DialogModalReferencaDePagoComponent implements OnInit {
  imagen$: Observable<any>;
  urlImagen:String="1"

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData,
               private angularFireStorage: AngularFireStorage,
               private movimientoService:MovimientoService) 
               { 

               }

  ngOnInit() {
    console.log(this.data.refenciaDePago, this.data.idMovimiento)
  }

  updateFaile(event) {
    const file = event.target.files[0];
    const dir = file.name;
    const fileRef = this.angularFireStorage.ref(dir);
    console.log(dir,"---dir---")
    console.log(file,"--file--s")
    const taskSubirImagen = this.angularFireStorage.upload(dir, file);
    taskSubirImagen.snapshotChanges()
    .pipe(
         finalize(() => {
          this.imagen$ = fileRef.getDownloadURL();
          this.imagen$.subscribe(url => {
            this.data.refenciaDePago.comprobantePago=url
            this.urlImagen=url
             console.log('URL::::',url)
             //this.form.get('imagen').setValue(url);
          });
         })
    )  
    .subscribe();
    console.log(file);
  }

  abjuntarArchivo(){
    this.data.refenciaDePago[0].comprobantePago=this.urlImagen
    console.log(this.data.refenciaDePago)
    this.movimientoService.updateFormasDePagoByMovimiento(this.data.refenciaDePago)
                           .subscribe(response=>{
                             console.log('anjuntado archivo')
                             alert("Comprobante enviados");
                             this.updateStatus();
                           }); 
  }

  updateStatus(){
    const STATUS={"status":"PAGADO"}
    this.movimientoService.updateStatusMovimiento(STATUS,this.data.idMovimiento)
                          .subscribe(response=>{
                            alert("Status pagado actualizado");
                            console.log(response,"Status pagado actualizado")
                          })

  }

}
