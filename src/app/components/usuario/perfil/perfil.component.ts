import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../../core/service/auth.service";
import { Router } from "@angular/router";
import { Usuario } from "src/app/core/model/usuario";
import { MovimientoService } from "@core/service/movimiento.service";
import { Movimiento } from "@core/model/movimiento";
import { MatDialog } from '@angular/material/dialog';
import { DialogModalReferencaDePagoComponent } from '../perfil/dialogModal/dialog-modal-referenca-de-pago/dialog-modal-referenca-de-pago.component'

@Component({
  selector: "app-perfil",
  templateUrl: "./perfil.component.html",
  styleUrls: ["./perfil.component.scss"]
})
export class PerfilComponent implements OnInit {
  usuario: Usuario = new Usuario();
  movimientos: Movimiento[] = [];
  panelOpenState = false;
  refenciaDePago:any=[];

  btnCancelarSi=false;
  btnCancelarNo=false;
  avatar:string ='';
  usuarioLogin:string ='';
  correoLogin:string ='';

  constructor(
    private authService: AuthService,
    private movimientoService: MovimientoService,
    private router: Router,
    public dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.avatar = window.localStorage.getItem('avatar')
    this.usuarioLogin = window.localStorage.getItem('usuario');
    this.correoLogin = window.localStorage.getItem('email');
    this.fechtUsuarioByEmail();
    this.fecthMovimientosByUsuario(); 
  }
  logout() {
    this.authService.logout().then(() => {
      localStorage.removeItem("sucursalId");
      localStorage.removeItem("usuarioId");
      localStorage.removeItem("sucursal");
      localStorage.removeItem("usuario");
      localStorage.removeItem("usuarioId");
      this.authService.addUsuarioLogeado(null);
      this.router.navigate(["/productos"]);
    });
  }

  fechtUsuarioByEmail() {
    this.authService
      .getUserByEmail(localStorage.getItem("email"))
      .subscribe(usuario => {
        this.usuario = usuario;
      });
  }

  fecthMovimientosByUsuario() {
    this.movimientoService
      .getByClienteProveedor(localStorage.getItem("email"))
      .subscribe(movimientos => {
        this.movimientos = movimientos;
      });
  }

  fecthFormaDePagoPorMovimiento(id){
    this.movimientoService.getFormaDePagoByMovimiento(id).subscribe(
      formaDePago=>{
        this.refenciaDePago=formaDePago
        this.openDialogFinalizarCompra("Referencia de pago",this.refenciaDePago,id);
        console.log(formaDePago)
      }
    )
  }
  
  openDialogFinalizarCompra(title,movimiento,idMovimiento) {
   const dialogRef = this.dialog.open(DialogModalReferencaDePagoComponent,{
      height:'600px',
      width:'400px',
      data: {title:title, refenciaDePago:movimiento,idMovimiento:idMovimiento}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.fecthMovimientosByUsuario();
    });

  }
  
  cancelarCompra=(id)=>{
   console.log(id,"id movimiento")
   const STATUS={"status":"CANCELADO"}
   this.movimientoService.updateStatusMovimiento(STATUS,id).subscribe(
                 response=>{
                   console.log(response)
                   alert("Movimiento Cancelado")
                   this.fecthMovimientosByUsuario();
                 },
                 error=>{
                   console.log("error",error)
                 }
                );
   }
}
