import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../core/service/auth.service';
import { FormBuilder, FormGroup, Validator, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { Usuario } from '../../../core/model/usuario';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.scss']
})
export class UsuarioComponent implements OnInit {
  formlogin: FormGroup;
  formcrearuser: FormGroup;
  messageCrearUsuario: string;
  usuario: Usuario;
  hide = true;

  messagelogin: string;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.builbFormLogin();
    this.builbFormUserRegirter();
  }

  private builbFormLogin() {
    this.formlogin = this.formBuilder.group({
     email : ['', [Validators.required, Validators.email]],
     password: ['', [Validators.required]]
    });
  }

  private builbFormUserRegirter() {
    this.formcrearuser = this.formBuilder.group({
     username: ['', Validators.required],
     email : ['', [Validators.required,Validators.email]],
     password: ['', Validators.required],
     enabled: [true],
     tipousuario: ['CLIENTE']
    });
  }

  login(event: Event) {
   event.preventDefault();
   if (this.formlogin.valid) {
        const value = this.formlogin.value;
        this.authService.login(value.email, value.password)
        .then(() => {
          console.log('Autentifico firebase');
          this.authService.getUserByEmail(value.email)
          .subscribe((usuario) => {
            console.log('Autentifico api curiel perfil');
            console.log(usuario);
            window.localStorage.setItem('usuarioId', usuario.id.toPrecision());
            window.localStorage.setItem('usuario', usuario.username);
            window.localStorage.setItem('email', usuario.email);
            this.authService.addUsuarioLogeado(usuario.username);
            this.router.navigate(['/perfil']);
          });
        })
        .catch((error) => {
          const errorEmail="There is no user record corresponding to this identifier. The user may have been deleted."
          const errorPassword="The password is invalid or the user does not have a password.";
          const mesageEmail="No se encontro ningun usuario con ese correo, Favor de registrase";
          const mesagaPassword="La contraseña es incorrecta";
          if(error.message==errorEmail)
             this.messagelogin = mesageEmail
          else if(error.message==errorPassword)
                  this.messagelogin = mesagaPassword
          else 
             this.messagelogin=error.message           
          console.log(error);
         });
      }
  }

  
  register(event: Event) {
    event.preventDefault();
    if (this.formcrearuser.valid) {
      const value = this.formcrearuser.value;
      console.log(this.formcrearuser.value);
      this.authService.createUser(value.email, value.password)
      .then((creado) => {
           console.log('creado firebase');
           this.authService.crearUsuarioCliente(this.formcrearuser.value).subscribe((user) => {
                  console.log('creado en postgresql');
                  console.log("usuario creador,",user)
                  window.localStorage.setItem('usuarioId', user.id.toPrecision());
                  window.localStorage.setItem('usuario', user.username);
                  window.localStorage.setItem('email', user.email);
                  this.authService.addUsuarioLogeado(user.username);
                  this.router.navigate(['/']);
           });
       })
      .catch((error) => {
         if(error.message=="Password should be at least 6 characters")
            this.messageCrearUsuario="La contraseña debe tener al menos 6 caracteres"
         if(error.message=="The email address is already in use by another account.")
            this.messageCrearUsuario="La dirección de correo electrónico ya está en uso por otra cuenta"
         else 
            this.messageCrearUsuario==error.message
          console.log(error);
      });
    }
  }
}
