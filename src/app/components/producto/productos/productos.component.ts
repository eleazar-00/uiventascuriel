import { Component, OnInit } from "@angular/core";
import { Producto } from "../../../core/model/producto.model";
import { ProductoService } from "../../../core/service/producto.service";
import { CategoriaService } from "../../../core/service/categoria.service";
import { Categoria } from "src/app/core/model/categoria";
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators
} from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { PageEvent } from "@angular/material/paginator";

@Component({
  selector: "app-productos",
  templateUrl: "./productos.component.html",
  styleUrls: ["./productos.component.scss"]
})
export class ProductosComponent implements OnInit {
  // form busqueda
  fromBusqueda: FormGroup;

  // datos pogress bar
  color = "primary";
  mode = "indeterminate";
  value = 50;

  productos: Producto[];
  categorias: Categoria[] = [];
  categoriaControl = new FormControl();
  mensageFechtProductos: string;

  // MatPaginator Inputs
  page = 0;
  length = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [10];

  // MatPaginator Output
  pageEvent: PageEvent;

  categoriaSeleccionado: number;
  nombreProducto: string;

  constructor(
    private productoService: ProductoService,
    private categoriaService: CategoriaService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}
  ngOnInit() {
    this.fechtCategorias();
    this.buildFormBusqueda();
    this.activatedRoute.paramMap.subscribe(params => {
      console.log("params");
      console.log(params);
      console.log("tipoConsula");
      this.page = +params.get("page");
      const idsucursal = +params.get("id");
      if (idsucursal) {
        this.fetchBySurcursalIdAndStatus(idsucursal, this.page);
      }
      if (this.page) {
        if (
          localStorage.getItem("tipoConsultaPaginaProductos") === "productos"
        ) {
          console.log("entron en productos");
          this.fetchProductos(this.page, this.pageSize);
        }
        if (
          localStorage.getItem("tipoConsultaPaginaProductos") === "categoria"
        ) {
          console.log("entro en categorias");
          const categoriaid = localStorage.getItem("categoriaIdSeleccionado");
          console.log(categoriaid);
          this.fetchByCategoriaIdAndStatus(categoriaid, this.page);
        }
        if (
          localStorage.getItem("tipoConsultaPaginaProductos") === "sucursal"
        ) {
          console.log("entro en sucursalid");
          const sucursalid = localStorage.getItem("sucursalIdSeleccionado");
          console.log(sucursalid);
          this.fetchBySurcursalIdAndStatus(sucursalid, this.page);
        }
      }
      if (!this.page && !idsucursal) {
        console.log("entron en producto sin paage y sin id sucursal");
        console.log(this.page);
        console.log(idsucursal);
        this.fetchProductos(this.page, this.pageSize);
      }
    });
  }
  buildFormBusqueda() {
    this.fromBusqueda = this.formBuilder.group({
      inputBusqueda: ["", Validators.required]
    });
  }
  onPaginateChange(event) {
    console.log("index console =" + event.pageIndex);
    this.router.navigate(["/productos/page/" + event.pageIndex]);
  }

  fetchProductos(page, pageSize) {
    this.productoService.getAll(page, pageSize, 1, true).subscribe(producto => {
      this.productos = producto.content;
      this.length = producto.totalElements;
      if (this.length <= 0) {
        this.mensageFechtProductos = " No se encontro ningun producto ";
      } else {
        localStorage.setItem("tipoConsultaPaginaProductos", "productos");
      }
    });
  }
  clickProducto(event) {}

  fetchByCategoriaIdAndStatus(categoriaId, page: number) {
    this.productoService
      .getByCategoriaIdAndStatus(categoriaId, page, this.pageSize, 1, true)
      .subscribe(producto => {
        this.productos = producto.content;
        this.mensageFechtProductos = "";
        this.length = producto.totalElements;
        if (this.length <= 0) {
          this.mensageFechtProductos =
            " No se encontro ningun producto con la categoria seleccionada ";
        } else {
          localStorage.setItem("categoriaIdSeleccionado", categoriaId + "");
          localStorage.setItem("tipoConsultaPaginaProductos", "categoria");
        }
      });
  }

  fetchBySurcursalIdAndStatus(sucursalId, page: number) {
    this.productoService
      .getBySucursalIdAndStatus(sucursalId, page, this.pageSize, 1, true)
      .subscribe(producto => {
        this.productos = producto.content;
        this.mensageFechtProductos = "";
        this.length = producto.totalElements;
        if (this.length <= 0) {
          this.mensageFechtProductos =
            " No se encontro ningun producto de la sucursal ";
        } else {
          localStorage.setItem("sucursalIdSeleccionado", sucursalId + "");
          localStorage.setItem("tipoConsultaPaginaProductos", "sucursal");
        }
      });
  }

  fetchByNombreAndStatus(nombre: string, page: number) {
    this.productoService
      .getByNombreAndStatus(nombre.toUpperCase(), page, this.pageSize, 1, true)
      .subscribe(producto => {
        this.productos = producto.content;
        console.log(this.productos, 'this.productos');
        this.mensageFechtProductos = "";
        this.length = producto.totalElements;
        if (this.length <= 0) {
          this.mensageFechtProductos = `No se encontro ningun producto refetente al nombre ${nombre}`;
        } else {
          localStorage.setItem("tipoConsultaPaginaProductos", "nombre");
        }
      });
  }
  btnbuscarPorNombre(event: Event) {
    event.preventDefault();
    this.fetchByNombreAndStatus(this.fromBusqueda.value.inputBusqueda, 0);
  }

  fechtCategorias() {
    this.categoriaService.getCategorias(1).subscribe(categoria => {
      this.categorias = categoria;
    });
  }
}
