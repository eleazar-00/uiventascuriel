import { Component, 
  Input, 
  Output, 
  EventEmitter, 
  OnInit } from '@angular/core';
import { Producto } from '../../../core/model/producto.model';
import { CartService } from '../../../core/service/cart.service';
import { Router, } from "@angular/router";
import {MatSnackBar,MatSnackBarHorizontalPosition,MatSnackBarVerticalPosition} from '@angular/material/snack-bar';
import {AlertComponent} from '../../../util/components/alert/alert.component'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.scss']
})
export class ProductoComponent implements OnInit {
  @Input() producto: Producto;
  @Output() productoClicked: EventEmitter<any> = new EventEmitter();
   durationInSeconds = 1;
   horizontalPosition: MatSnackBarHorizontalPosition = 'center';
   verticalPosition: MatSnackBarVerticalPosition = 'top';
  constructor(
    private cartService: CartService,
    private router:Router,
    private _snackBar: MatSnackBar,
    private toastrService: ToastrService
  ) {
    console.log('constructor', this.producto);
   }
  ngOnInit() {
    console.log('3. ngInit');
  }
  addCart() {
    console.log('añadir al carrtio',this.producto); 
    this.cartService.addCart(this.producto);
    this.toastrService.success(`Producto: ${this.producto.nombre}` ,"Se agrego un producto al carrito")
  }
  openSnackBar() {
    this._snackBar.openFromComponent(AlertComponent, {
      duration: this.durationInSeconds * 1000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: ['example-pizza-party']
    });
  }

  comprar(){
    this.cartService.addCart(this.producto);
    this.router.navigate(["/order"]);
  }

}
