import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import { from } from 'rxjs';
import { ProductoService } from '../../../core/service/producto.service';
import { Producto } from 'src/app/core/model/producto.model';
import {CartService} from "@core/service/cart.service";

@Component({
  selector: 'app-producto-detalle',
  templateUrl: './producto-detalle.component.html',
  styleUrls: ['./producto-detalle.component.scss']
})
export class ProductoDetalleComponent implements OnInit {
  producto: Producto;
  constructor(
    private cartService:CartService,
    private route: ActivatedRoute,
    private router:Router,
    private productoService: ProductoService
  ) {
   }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
        const id = params.id;
        this.fetchProducto(id);
       });
  }
  addCart=()=> {
    console.log('añadir al carrtio',this.producto);
    this.cartService.addCart(this.producto);
  }
  comprar(){
    this.cartService.addCart(this.producto);
    this.router.navigate(["/order"]);
  }

  fetchProducto(id: number) {
    this.productoService.getById(id)
    .subscribe(producto => {
      this.producto = producto;
      console.log(producto);
    });
  }
  /*
  updateProducto() {
    const producto: Partial<Producto> = {
      imagen: 'assets/images/banner-2.jpg',
      precio: 2000 };
    this.productoService.update('9', producto)
    .subscribe( productos => {
    console.log(productos);
    });
    }
    deleteProducto() {
      this.productoService.delete('9')
      .subscribe(rtp => {
        console.log(rtp);
      });
    }*/

}
