import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Sucursal } from 'src/app/core/model/sucursal';
import { SucursalService } from '../../../core/service/sucursal.service';

@Component({
  selector: 'app-sucursal',
  templateUrl: './sucursal.component.html',
  styleUrls: ['./sucursal.component.scss']
})
export class SucursalComponent implements OnInit {

  @Input() sucursal: Sucursal; 
  @Output() sucursalClicked: EventEmitter<any> = new EventEmitter();
  constructor(
    private sucursalService: SucursalService
  ) { }

  ngOnInit() {
  }

  meGusta() {
    this.sucursalService.meGusta(this.sucursal.id)
    .subscribe(rpt => {
      this.sucursal.meGusta = this.sucursal.meGusta + rpt;
    });
  }

}
