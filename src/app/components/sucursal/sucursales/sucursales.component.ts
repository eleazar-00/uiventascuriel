import { Component, OnInit } from '@angular/core';
import { SucursalService } from '../../../core/service/sucursal.service';
import { Sucursal } from 'src/app/core/model/sucursal';

@Component({
  selector: 'app-sucursales',
  templateUrl: './sucursales.component.html',
  styleUrls: ['./sucursales.component.scss']
})
export class SucursalesComponent implements OnInit {
  color = 'primary';
  mode = 'indeterminate';
  value = 50;
  sucursales: Sucursal[];
  constructor(
    private sucursalService: SucursalService
  ) { }

  ngOnInit() {
   this.getAllSucursal();
  }
 getAllSucursal() {
  this.sucursalService.getAll()
  .subscribe(sucursales => {
    this.sucursales = sucursales;
  });
 }

}
