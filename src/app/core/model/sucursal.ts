export class Sucursal {
    id: number;
    nombre: string;
    status: number;
    contacto: string;
    redSocial: string;
    meGusta: number;
    logo: string;
    usuario: number;
    descripcion: string;
    fechaCreacion: Date;
    fechaModificacion: Date;
}
