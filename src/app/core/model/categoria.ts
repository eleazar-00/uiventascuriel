export class Categoria {
    id: number;
    descripcion: string;
    status: number;
    categorias: Categoria[];
}
