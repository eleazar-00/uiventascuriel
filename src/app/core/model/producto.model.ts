import { Sucursal } from './sucursal';
import { Categoria } from './categoria';
import { Unidad } from './unidad';

export interface Producto {
    id: number;
    nombre: string;
    descripcion: string;
    codigoBarras:string;
    status: number;
    precio: number;
    costo: number;
    cantidad: number;
    unidadMedida: Unidad;
    imagen: string;
    fechaCreacion: string;
    fechaModificacion: string;
    sucursal: Sucursal;
    categoria: Categoria;
    existencias: number;
    apartados: number;
    palabrasClave: string;
    observaciones :string;
 }