import { Producto } from "./producto.model";

export class DetalleMov {
  id: number;
  cantidad: number;
  importe: number;
  producto: any;
}
