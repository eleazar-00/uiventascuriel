import { DetalleMov } from "@core/model/detalleMov";

export class Movimiento {
  id: number;
  descripcion: string;
  observacion: string;
  clienteProveedor: string;
  detallesMov: DetalleMov[];
  movimientoFormaPago:any;
  createAt: Date;
  status:String;
  system:String;
  total: number;
}
