export class Usuario {
    id: number;
    username: string;
    email: string;
    password: string;
    enabled: boolean;
    tipousuario: string;
    sucursalid: number;
}
