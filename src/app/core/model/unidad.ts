export class Unidad {
    id: number;
    clave: string;
    descripcion: string;
    status: number;
    usuario: number;
    fechaCreacion: Date;
    fechaModificacion: Date;

}
