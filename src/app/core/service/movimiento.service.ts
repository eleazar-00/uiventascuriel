import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Movimiento } from "@core/model/movimiento";
import { environment } from "../../../environments/environment";
import { retry } from 'rxjs/operators';

@Injectable({
  providedIn: "root"
})
export class MovimientoService {
  constructor(private httpClient: HttpClient) {}

  create(movimiento: Movimiento) {
    return this.httpClient.post<any>( `${environment.url_api_ventas_curiel}/api/v1/movimientos`, movimiento );
  }
  getAll(size, page) {
    return this.httpClient.get(`${environment.url_api_ventas_curiel}/api/v1/movimientos`, {params:{size,page}});
  }

  getByClienteProveedor(clienteproveedero: string) {
    return this.httpClient.get<Movimiento[]>(
      `${environment.url_api_ventas_curiel}/api/v1/movimientos/clientes?clienteProveedor=${clienteproveedero}`
    );
  }
  getFormaDePagoByMovimiento=(id)=>{
    return this.httpClient.get<any>(`${environment.url_api_ventas_curiel}/api/v1/pagos/movimientos/${id}`);
  }
  
  updateFormasDePagoByMovimiento=(referencias)=>{
    return this.httpClient.put<any>(`${environment.url_api_ventas_curiel}/api/v1/pagos/movimientos`,referencias[0]);
  }

  updateStatusMovimiento=(status,id)=>this.httpClient.patch<any>(`${environment.url_api_ventas_curiel}/api/v1/movimientos/${id}`,status);

  createExcel=()=>(this.httpClient.get<any>(`${environment.url_api_ventas_curiel}/api/v1/movimientos/download.xlsx`))
}
