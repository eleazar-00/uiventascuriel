import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Sucursal } from '../model/sucursal';

@Injectable({
  providedIn: 'root'
})
export class SucursalService {

  constructor(
    private http: HttpClient
  ) { }

  getAll() {
    return this.http.get<Sucursal[]>(`${environment.url_api_ventas_curiel}/api/v1/sucursales`);
  }

  meGusta(id: number) {
     return this.http.put<number>(`${environment.url_api_ventas_curiel}/api/v1/sucursales/${id}/megusta`, null);
  }

  getSucursalById(id: number) {
    return this.http.get<Sucursal>(`${environment.url_api_ventas_curiel}/api/v1/sucursales/${id}`);
  }
}
