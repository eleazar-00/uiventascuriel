import { Injectable } from '@angular/core';
import { HttpClient} from "@angular/common/http"
import {FormaPago} from "@core/model/forma-pago"
import { environment } from "../../../environments/environment"
@Injectable({
  providedIn: 'root'
})
export class FormaPagoService {

  constructor(private httpClient:HttpClient) { }

  getFormasDePagoByEstatus=()=>{
    return this.httpClient.get<FormaPago[]>(`${environment.url_api_ventas_curiel}/api/v1/formasdepago`);
  }

  getReferenciaFormaDePago=(id)=>this.httpClient.get<any>(`${environment.url_api_ventas_curiel}/api/v1/referenciaspago/${id}/formasdepago`);
}
