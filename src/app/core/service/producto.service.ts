import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {Producto} from '../../core/model/producto.model';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  constructor(private http: HttpClient) { }

  productos: Producto[] = [];

  getAll(page: number, pageSize: number, status: number, visiblePage: boolean) {
    return this.http.get<any>( `${environment.url_api_ventas_curiel}/api/v1/productos/page?asc=true&order=nombre&page=${page}&size=${pageSize}&status=${status}&visiblePage=${visiblePage}`);
  }

  getById(id: number) {
    return this.http.get<Producto>(`${environment.url_api_ventas_curiel}/api/v1/productos/${id}`);
   }

   getByCategoriaIdAndStatus(id: number, page: number, pageSize: number, status: number, visiblePage: boolean) {
    const endpoint = `${id}/productos?asc=true&order=nombre&page=${page}&size=${pageSize}&status=${status}&visiblePage=${visiblePage}`;
    return this.http.get<any>(`${environment.url_api_ventas_curiel}/api/v1/categorias/${endpoint}`);
   }

   getBySucursalIdAndStatus(id: number, page: number, pageSize: number, status: number, visiblePage: boolean) {
    const endpoint = `${id}/productos?asc=true&order=nombre&page=${page}&size=${pageSize}&status=${status}&visiblePage=${visiblePage}`;
    return this.http.get<any>(`${environment.url_api_ventas_curiel}/api/v1/sucursales/${endpoint}`);
   }

  getByNombreAndStatus(nombre: string, page: number, pageSize: number, status: number, visiblePage: boolean) {
    const endpoint = `asc=true&nombre=${nombre}&order=nombre&page=${page}&size=${pageSize}&status=${status}&visiblePage=${visiblePage}`;
    return this.http.get<any>(`${environment.url_api_ventas_curiel}/api/v1/productos/like?${endpoint}`);
  }
  create(producto: Producto) {
      producto.status = Number(producto.status);
   return this.http.post<Producto>(`${environment.url_api_ventas_curiel}/api/v1/productos`, producto);
  }

  update(id: string, changes: Partial<Producto> ) {
    return this.http.put(`${environment.url_api}/products/${id}`, changes);
  }

  delete(id: string ) {
    return this.http.delete(`${environment.url_api_ventas_curiel}/api/v1/productos/${id}`);
  }
  getByCodigodbarras(codigoBarras : string) {
    return this.http.get<Producto>(`${environment.url_api_ventas_curiel}/api/v1/productos/buscar`,{params:{codigoBarras}});
  }
}
