import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { from } from 'rxjs';
import { environment} from '../../../environments/environment';
import { Unidad } from '../model/unidad';

@Injectable({
  providedIn: 'root'
})
export class UnidadService {

  constructor(
    private  http: HttpClient
  ) {
  }

  getAll() {
    return this.http.get<Unidad[]>(`${environment.url_api_ventas_curiel}/api/v1/unidades`);
  }

}
