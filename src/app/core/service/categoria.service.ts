import { Injectable } from '@angular/core';
import { Categoria } from '../model/categoria';
import { HttpClient } from '@angular/common/http';
import { environment} from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  constructor(
    private http: HttpClient
  ) { }

  getAll() {
   return this.http.get<Categoria[]>(`${environment.url_api_ventas_curiel}/api/v1/categorias`);
  }

  getCategorias(status: number) {
    return this.http.get<Categoria[]>(`${environment.url_api_ventas_curiel}/api/v1/categorias/padres?status=${status}`);
   }
  getSubCategorias(id: number, status: number) {
    return this.http.get<Categoria[]>(`${environment.url_api_ventas_curiel}/api/v1/categorias/${id}/subcategoria?status=${status}`);
   }
}
