import { Injectable } from "@angular/core";
import { Producto } from "../model/producto.model";
import { BehaviorSubject } from "rxjs";
 
@Injectable({
  providedIn: "root"
})
export class CartService {
  private producto: Producto[] = [];
  
  private card = new BehaviorSubject<Producto[]>([]);
  cart$ = this.card.asObservable();

  constructor() {
    if(localStorage.getItem('orderOnline')) {
      this.producto = JSON.parse(localStorage.getItem('orderOnline'));
      this.card.next(this.producto);
     }
   }

  addCart(producto: Producto) { 
     let index=this.producto.findIndex(prodc => prodc.id === producto.id);
      if(index>=0){
        this.producto[index].cantidad = this.producto[index].cantidad +1;
      }      
     else{
      producto["cantidad"]=1;
      this.producto = [...this.producto, producto];
    }
    console.log(this.producto,"todo carrito")
    this.nextProductos(this.producto);
  }

  deleteCart(producto: Producto) { 
    let index=this.producto.findIndex(prodc => prodc.id === producto.id);
    console.log(index, 'delete');
    if(this.producto[index].cantidad==1){
        this.producto.splice(index,1);
     }else{
      this.producto[index].cantidad = this.producto[index].cantidad-1; 
    } 
    this.nextProductos(this.producto);
 }

  deleteRowCart(i) {
    this.producto.splice(i, 1);
    this.nextProductos(this.producto);
  }

  clearCart() {
    this.producto = [];
    this.nextProductos(this.producto);
  }

  nextProductos(productos) {
    localStorage.setItem('orderOnline',  JSON.stringify(productos));
    this.card.next(productos);
  }
}
