import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Categoria} from '@core/model/categoria';
import { environment } from "../../../environments/environment";


@Injectable({
  providedIn: 'root'
})
export class LineaService {
  constructor(private httpClient:HttpClient) { }
  
  saves=(categoria:Categoria)=>( this.httpClient.post<Categoria>(`${environment.url_api_ventas_curiel}/api/v1/categorias`,categoria))

  update=(categoria:Categoria)=>( this.httpClient.put<Categoria>(`${environment.url_api_ventas_curiel}/api/v1/categorias`,categoria))

  getAll=()=>( this.httpClient.get<any>(`${environment.url_api_ventas_curiel}/api/v1/categorias`))

  delete = (id) => (this.httpClient.delete(`${environment.url_api_ventas_curiel}/api/v1/categorias/${id}`))
   
  getAllPadres=()=> this.httpClient.get<any>(`https://apiventascuriel.herokuapp.com/api/v1/categorias/padres?status=1`)
}
