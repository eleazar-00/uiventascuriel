import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { environment } from '../../../environments/environment';
import { from } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {Usuario} from '../../core/model/usuario';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
   private mensajeUsuarioLogeado = new BehaviorSubject<string>('');
   mensajeUsuarioLogeado$ = this.mensajeUsuarioLogeado.asObservable();

  constructor(
    private af: AngularFireAuth,
    private http: HttpClient
  ) { }
  
  addUsuarioLogeado(mensaje) {
    this.mensajeUsuarioLogeado.next(mensaje);
  }

  createUser(email: string, password: string) {
    return this.af.auth.createUserWithEmailAndPassword(email, password);
  }
  login(email: string, password: string) {
     return this.af.auth.signInWithEmailAndPassword(email, password);
  }

  loginGoogle(){
    return  this.af.auth.signInWithPopup(new auth.GoogleAuthProvider())
  }

  loginFaceboock() {
    return this.af.auth.signInWithPopup(new auth.FacebookAuthProvider());
  }
  logout() {
     return this.af.auth.signOut();
  }
 
  hasuser() {
    return this.af.authState;
  }

  crearUsuarioCliente(usuario: Usuario) {
    return this.http.post<Usuario>(`${environment.url_api_ventas_curiel}/api/v1/usuarios`, usuario);
  }

  getUserByEmail(email: string){
    return this.http.get<Usuario>(`${environment.url_api_ventas_curiel}/api/v1/usuarios/email?email=${email}`);
  }
}
