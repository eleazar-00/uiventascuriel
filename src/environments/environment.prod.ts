export const environment = {
  production: true,
  url_api: 'https://platzi-store.herokuapp.com',
  url_api_ventas_curiel: 'https://apiventascuriel.herokuapp.com',
  firebase: {
    apiKey: 'AIzaSyCQq4NuCPkoTS73e_LCh0-Bf3OgqevvF4Y',
    authDomain: 'mercadito-fff0d.firebaseapp.com',
    databaseURL: 'https://mercadito-fff0d.firebaseio.com',
    projectId: 'mercadito-fff0d',
    storageBucket: 'mercadito-fff0d.appspot.com',
    messagingSenderId: '601157140582',
    appId: '1:601157140582:web:f91cdc6fbd93103e83eda7'
  }
};
