// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url_api: 'https://platzi-store.herokuapp.com',
  url_api_ventas_curiel: 'http://localhost:8080',
  firebase: {
    apiKey: 'AIzaSyCQq4NuCPkoTS73e_LCh0-Bf3OgqevvF4Y',
    authDomain: 'mercadito-fff0d.firebaseapp.com',
    databaseURL: 'https://mercadito-fff0d.firebaseio.com',
    projectId: 'mercadito-fff0d',
    storageBucket: 'mercadito-fff0d.appspot.com',
    messagingSenderId: '601157140582',
    appId: '1:601157140582:web:f91cdc6fbd93103e83eda7'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
